package nugraha.aditya.gudanggaram

import android.database.sqlite.SQLiteDatabase
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View

import androidx.fragment.app.FragmentTransaction
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BottomNavigationView.OnNavigationItemSelectedListener {

    lateinit var db : SQLiteDatabase
    lateinit var fragBrg : FragmentBrg
    lateinit var fragJenis : FragmentJenis
    lateinit var ft : FragmentTransaction

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.itemAbout -> frameLayout.visibility = View.GONE
            R.id.itemBarang ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragBrg).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
            R.id.itemJenis ->{
                ft = supportFragmentManager.beginTransaction()
                ft.replace(R.id.frameLayout,fragJenis).commit()
                frameLayout.setBackgroundColor(Color.argb(245,255,255,255))
                frameLayout.visibility = View.VISIBLE
            }
        }
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        bottomNavigationView.setOnNavigationItemSelectedListener(this)
        fragJenis = FragmentJenis()
        fragBrg = FragmentBrg()
        db = DBOpenHelper(this).writableDatabase
    }
    fun getDbObject() : SQLiteDatabase{
        return db
    }
}
                                                                                                                                                                                                                                                                                                                                                                                     