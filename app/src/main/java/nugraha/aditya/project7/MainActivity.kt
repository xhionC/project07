package nugraha.aditya.project7

import android.app.AlertDialog
import android.content.Intent
import android.content.ContentValues
import android.database.sqlite.SQLiteDatabase
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.database.Cursor
import android.view.View
import com.google.zxing.BarcodeFormat
import com.google.zxing.integration.android.IntentIntegrator
import com.journeyapps.barcodescanner.BarcodeEncoder
import android.widget.CursorAdapter
import kotlinx.android.synthetic.main.activity_main.*
import android.content.DialogInterface
import android.widget.ListAdapter
import android.widget.SimpleCursorAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.item_data_mhs.*
import java.util.*

class MainActivity : AppCompatActivity(), View.OnClickListener {
    lateinit var lsAdapter: ListAdapter
    lateinit var db: SQLiteDatabase
    lateinit var intentIntegrator: IntentIntegrator
    lateinit var dialog: AlertDialog.Builder
    override fun onClick(v: View?) {

        when (v?.id) {
            R.id.btn1 -> {
                intentIntegrator.setBeepEnabled(true).initiateScan()
            }
            R.id.btn2 -> {
                val barCodeEnCoder = BarcodeEncoder()
                val bitmap = barCodeEnCoder.encodeBitmap(
                    edQrCode.text.toString(),
                    BarcodeFormat.QR_CODE, 400, 400
                )
                imV.setImageBitmap(bitmap)
            }
            R.id.btn3 -> {
                dialog.setTitle("Konfirmasi").setIcon(android.R.drawable.ic_dialog_info)
                    .setMessage("Apakah data yang dimasukkan sudah benar?")
                    .setPositiveButton("Ya", btnInsertDialog)
                    .setNegativeButton("Tidak", null)
                dialog.show()
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        val intentResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)
        if (intentResult != null) {
            if (intentResult.contents != null) {
                edQrCode.setText(intentResult.contents)
                val strToken = StringTokenizer(edQrCode.text.toString(), "-", false)
                edNim.setText(strToken.nextToken())
                edNama.setText(strToken.nextToken())
                edJurusan.setText(strToken.nextToken())
            } else {
                Toast.makeText(this, "Dibatalkan", Toast.LENGTH_SHORT).show()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        intentIntegrator = IntentIntegrator(this)
        dialog = AlertDialog.Builder(this)
        btn1.setOnClickListener(this)
        btn2.setOnClickListener(this)
        btn3.setOnClickListener(this)
        db = BDOpenHelper(this).writableDatabase
    }

    override fun onStart() {
        super.onStart()
        showDataMhs()
    }


    fun showDataMhs() {
        val cursor: Cursor = db.query(
            "mhs", arrayOf("nim as _id", "nama", "jurusan"),
            null, null, null, null, "_id asc"
        )
        lsAdapter =
            SimpleCursorAdapter(
                this, R.layout.item_data_mhs, cursor,
                arrayOf("_id", "nama", "jurusan"), intArrayOf(R.id.txNim, R.id.txNama, R.id.txJurusan),
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER
            ) as ListAdapter
        lv1.adapter = lsAdapter


    }

    fun insertDataMhs(nim: String, nama: String, jurusan: String) {
        var cv: ContentValues = ContentValues()
        cv.put("nim", nim)
        cv.put("nama", nama)
        cv.put("jurusan", jurusan)
        db.insert("mhs", null, cv)
        showDataMhs()
    }


    val btnInsertDialog = DialogInterface.OnClickListener { dialog, which ->
        insertDataMhs(
            edNim.text.toString(),
            edNama.text.toString(),
            edJurusan.text.toString()
        )
        edNim.setText("")
        edNama.setText("")
        edJurusan.setText("")
        edQrCode.setText("")
    }
}
